# -*- coding: utf-8 -*-
# Generated by Django 1.10 on 2016-08-25 11:47
from __future__ import unicode_literals

from django.db import migrations, models
import picklefield.fields


class Migration(migrations.Migration):

    initial = True

    dependencies = [
    ]

    operations = [
        migrations.CreateModel(
            name='DjangoRunQueue',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('TaskName', models.CharField(max_length=100)),
                ('TaskQueue', models.CharField(max_length=100)),
                ('TaskArgs', picklefield.fields.PickledObjectField(editable=False)),
                ('TaskResults', picklefield.fields.PickledObjectField(editable=False)),
                ('Proccessed', models.BooleanField(default=False)),
            ],
        ),
    ]
